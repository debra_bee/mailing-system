import json
import boto3
from botocore.exceptions import ClientError

ses = boto3.client('ses', region_name="us-east-1")

def lambda_handler(event, context):
    # TODO implement
    try:
        response = ses.send_email(
            Source = "debbymags@gmail.com",
            Destination={
                'ToAddresses': [
                    "debbymags@gmail.com"
                ]
            },
            Message={
                'Subject': {
                    'Data': "test"
                },
                'Body': {
                    'Text': {
                        'Data': "test email"
                    }
                }
            }
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])
    
    return {
        "statusCode": 200,
        "body": json.dumps('Hello from Lambda!')
    }
